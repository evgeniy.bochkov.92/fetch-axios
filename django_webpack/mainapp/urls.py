from django.urls import path, include
from rest_framework.routers import DefaultRouter

from mainapp import views
from mainapp.views import CourseViewSet, StudentViewSet

router = DefaultRouter()
router.register("courses", CourseViewSet)
router.register("students", StudentViewSet)

urlpatterns = [
    path('test/', views.test),
    path('api/', include(router.urls)),
    path('api-view/', views.api_view),
]