from rest_framework import serializers

from mainapp.models import Course, Student


class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = "id", "name"
        view_name = "courses"


class StudentSerializer(serializers.ModelSerializer):
    courses = CourseSerializer(many=True, required=False)

    class Meta:
        model = Student
        fields = "id", "first_name", "second_name", "last_name", "courses"
        view_name = "students"
