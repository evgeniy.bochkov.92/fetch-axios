from django.http import JsonResponse
from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet

from mainapp.models import Course, Student
from mainapp.serializers import CourseSerializer, StudentSerializer


def test(request):
    return render(request, "mainapp/test.html")


def api_view(request):
    return JsonResponse({'hello': 'world'})


class CourseViewSet(ModelViewSet):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer
    permission_classes = [IsAuthenticated]


class StudentViewSet(ModelViewSet):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer
    permission_classes = [IsAuthenticated]
